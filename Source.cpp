#include <math.h>
#include <map>
#include <vector>
#include <iostream>
using namespace std;

char pistring[] = "314159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214808651328230664709384460955058223172535940812848111745028410270193852110555964462294895493038196442881097566593344612847564823378678316527120190914564856692346034861045432664821339360726024914127372458700660631558817488152092096282925409171536436789259036001133053054882046652138414695194151160943305727036575959195309218611738193261179310511854807446237996274956735188575272489122793818301194912983367336244065664308602139494639522473719070217986094370277053921717629317675238467481846766940513200056812714526356082778577134275778960917363717872146844090122495343014654958537105079227968925892354201995611212902196086403441815981362977477130996051870721134999999837297804995105973173281609631859502445945534690830264252230825334468503526193118817101000313783875288658753320838142061717766914730359825349042875546873115956286388235378759375195778185778053217122680661300192787661119590921642019893809525720106548586327886593615338182796823030195203530185296899577362259941389124972177528347913151557485724245415069595";

std::map<char, std::vector<unsigned char> > dict;

std::string makeSerial( std::vector<unsigned char> s)
{
    std::vector<unsigned char> rev;
    
    
}
// get index of character
int get_index(int charidx)
{
	return ((int)pow((float)charidx, 2) + (3 * charidx) - 2)/2;
}

// get character which has index = index
int get_alpha(int index)
{
	return ((int)sqrt((float)(8*index + 17)) - 3) / 2;
}

// generate table from static idx
void gen_table()
{
	char part[26+4];
	//char *endptr;

	for (int i = 1; i < 27; i++)
	{
		//loop through alphabets
		//printf("Character -> %c = ", 'a' + (i - 1)); //dbg

		//the values extracted from table.
		std::vector<unsigned char> value;

		//copy the values from table index to part variable
		strncpy(part, pistring + get_index(i) - 1, /*f(x) here for set Z*/ i + 2);

		//add the values to vector
		for (int j = 0; j < (i + 2); j++)
		{
            //printf("%i", part[j] - '0'); //dbg
			value.push_back(part[j] - '0');
		}
		//map alpha : value
		//dict.at( 'a'+ (i- 1) ) = value;
		dict['a' + (i - 1)] = value;

		//printf("\n"); //dbg
	}
}

int main()
{
	gen_table();
    
    //output gen table.
    for(auto it = dict.begin(); it != dict.end(); ++it)
    {
        cout << it->first << " : ";
        for( auto it2 : it->second)
        {
            printf("%i ", it2);
            //cout << it2 <<" ";
        }
        cout<< endl;
    }
    
    /*string name;
    vector<unsigned char> serial;
    
    cout << "Enter name: ";
    cin>> name;
    cout << "Enter serial: ";
*/
	// so now you can access the entire v for k:
	auto v = dict['f'];
	// and then each individual part of that as if it were an array:
	auto c = v[0];
	cout << c << endl;
	// or just straight like:
	auto d = dict['f'][0];
    printf("d -> %i\n", d);
	//cout << d << endl;
	// standard stl method for checking length...
	int l = dict['f'].size();
	cout << "length of f -> " << l << endl;
	return 0;
}
