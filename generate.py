import math

def gentable(tab):
    resalpha = []
    resindex = []
    
    index = 0
    for x in tab:
        resalpha.append(str(x))
        resindex.append(index+1)
        index = index +1
    result = dict(zip(resalpha,resindex))
    return result

    
def getindex(input):
    return (input**2+3*input-2)/2
    

def getalpha(index):
    return (math.sqrt(8*index + 17) - 3)/2
