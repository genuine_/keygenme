#include <stdint.h>
#include <iostream>
#include <limits>

typedef int64_t RsaInt;

// the extended euclidean algorithm
inline RsaInt Xgcd(RsaInt a, RsaInt b, RsaInt &x, RsaInt &y)
{
    x=0, y=1; 
    RsaInt u=1, v=0, m, n, q, r;
    RsaInt gcd = b;
    while (a != 0) 
    {
        //divisor
        q = gcd/a;
        //remainder
        r = gcd%a;
        
        m = x-u*q; 
        n = y-v*q;

        gcd = a;
        // assign a to remainder. condition for euclidean if gcd is 0, then the gcd is last divisor
        a = r; 
        x = u; 
        y = v; 
        u = m; 
        v = n;
    }

    return gcd;
}

inline RsaInt Modulo(RsaInt x, RsaInt m)
{
    return x < 0 ? ((x % m) + m) % m : x % m;
}

inline RsaInt MulInv(RsaInt a, RsaInt b)
{
    RsaInt x, y;
    if(Xgcd(a, b, x, y) != 1) 
    {
        std::cout << "a multiplicative inverse does not exist for " << a << " and " << b << std::endl;
        return 0;
    }

    return Modulo(x, b);
}

inline RsaInt RsaCrypt(RsaInt key, RsaInt msg, RsaInt n)
{
    // m^e (mod n)
    // use exponentiation by squaring

    RsaInt result = 1;
    while(key != 0)
    {
        if(key % 2 == 1)
        {
            // key is odd
            result = Modulo(result*msg, n);
        }
        msg = Modulo(msg*msg, n);
        key >>= 1;
    }

    return result;
}

int main()
{
    //p and q are prime numbers, p*q must not exceed 2^31
    RsaInt p = 32633, q = 65413;
    //public key 1
    RsaInt n = p*q;
    
    
    RsaInt phi = (p-1)*(q-1);
    std::cout << "phi is: " << phi << std::endl;
    //e is 1 < e < phi and is a coprime of 3120
    //public key 2
    RsaInt e = 65537;
    
    // secret key e * x = 1 (mod phi)
    RsaInt d = MulInv(e, phi);

    RsaInt plain = 69;

    std::cout << "d=" << d << " n=" << n << std::endl;
    std::cout << "plain=" << plain << std::endl;

    RsaInt cipher = RsaCrypt(e, plain, n);
    std::cout << "cipher=" << cipher << std::endl;

    RsaInt decrypted = RsaCrypt(d, cipher, n);
    std::cout << "decrypted=" << decrypted << std::endl;
}

