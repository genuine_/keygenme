Serial Generation using PI table.(for fun)

1.) Accept alphabets a-z (llowercase for now)
2.) Map alphabets to number table

so A[a..z] = I[1..27]
generate.py does this using gentable function
Simple transformation (could also be modified here)

So we have a table with over 400 of the PI digits. (the table has no specific purpose, it can be replaced with any kind of table really)

The next transformation is on the set I
where  f(x) = I + 2

which is now a set of Z[f(I)] where each letter now also has a corresponding Z[].

A 		I 		Z
a 		1 		3
b 		2 		4
c 		3 		5
d 		4 		6
e 		5 		7

so on and so forth

Here's where things get alittle interesting. The Z's purpose is to extract Z[] number of digits from the PI Table that corresponds to each letter. And the tricky part was always finding the right index to start from in the table, for instance.

let input = abc  (there will be a minimum limit of 6 character input or more...)

input = abc
Z[a] = 3
Z[b] = 4
Z[c] = 5

PI TABLE Extract:
31415926535897932384626433832795028841971693993751...

The letter a, starts the index at 1 (0 for programming). So a = 314
b grabs 4 digits after that, starting at index 4 of the table. which yields 1592

c grabs the next 5 digits after b, yielding 65258.

The only detail thats needed here is the index at which each character grabs from the PI Table.

After a hard fought battle with math using linear regression, we come up with the formula. 

let P be the index and x be the input P = (x^2 + 3x-2)/2
This equation was verified through the python code and a manual table constructed via Excel. (thanks to a good friend of mine who helped me derive the formula)

Now generating the A->Z table was easily done in python. And Im still brainstorming before I move forward to C/C++ and getting a prelimenary keygen coded.


genuine

