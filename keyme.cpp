#include <stdint.h>
#include <iostream>
#include <limits>
#include <string>
#include <sstream>
#include <vector>

typedef int64_t RsaInt;


inline RsaInt Modulo(RsaInt x, RsaInt m)
{
    return x < 0 ? ((x % m) + m) % m : x % m;
}

inline RsaInt RsaCrypt(RsaInt key, RsaInt msg, RsaInt n)
{
    // m^e (mod n)
    // use exponentiation by squaring
    
    RsaInt result = 1;
    while(key != 0)
    {
        if(key % 2 == 1)
        {
            // key is odd
            result = Modulo(result*msg, n);
        }
        msg = Modulo(msg*msg, n);
        key >>= 1;
    }
    
    return result;
}


inline std::vector<std::string> &Split(const std::string &s, char delim, std::vector<std::string> &elems)
{
    std::stringstream ss(s);
    std::string item;
    while(std::getline(ss, item, delim))
    {
        elems.push_back(item);
    }
    return elems;
}

inline RsaInt HexToInt(const std::string &s)
{
    RsaInt i;
    std::istringstream convert(s);
    convert >> std::hex >> i;
    return i;
}

inline bool VerifySerial(const std::string &serial, std::string &id)
{
    // format: d-n-length-data
    std::vector<std::string> parts;
    Split(serial, '-', parts);
    
    // requires 6 parts minimum
    if(parts.size() < 6)
    {
        return false;
    }
    
    std::string id1, id2;
    
    RsaInt d = HexToInt(parts[0]);
    RsaInt n = HexToInt(parts[1]);
    RsaInt len = RsaCrypt(d, HexToInt(parts[2]), n);
    
    if(len > 255)
    {
        return false;
    }
    
    id1.resize((std::string::size_type)len);
    
    for(std::string::size_type i=0;i<len;++i)
    {
        RsaInt cipher = HexToInt(parts[i+3]);
        id1[i] = (char)RsaCrypt(d, cipher, n);
    }
    
    d = HexToInt(parts[len+3]);
    n = HexToInt(parts[len+4]);
    RsaInt len2 = RsaCrypt(d, HexToInt(parts[len+5]), n);
    
    if(len != len2)
    {
        return false;
    }
    
    id2.resize((std::string::size_type)len2);
    
    for(std::string::size_type i=0;i<len2;++i)
    {
        RsaInt cipher = HexToInt(parts[i+len+6]);
        id2[i] = (char)RsaCrypt(d, cipher, n);
    }
    
    if(id1 == id2)
    {
        id = id1;
        return true;
    }
    else
        return false;
}

int main(int argc, char **argv)
{
    if(argc != 2)
    {
        std::cout << "Usage: keygenme [serial]" << std::endl;
    }
    else
    {
        std::string verified;
        if(VerifySerial(argv[1], verified))
        {
            std::cout << "Serial is verified for " << verified << std::endl;
        }
        else
        {
            std::cout << "Invalid serial TRY AGAIN!" << std::endl;
        }
    }
}